var searchData=
[
  ['savecandidats_0',['saveCandidats',['../deux_8c.html#a144c1ecbcf346d4181e31a558ba84ac5',1,'saveCandidats(ListeCandidats liste, int nbCandidats):&#160;deux.c'],['../deux_8h.html#a144c1ecbcf346d4181e31a558ba84ac5',1,'saveCandidats(ListeCandidats liste, int nbCandidats):&#160;deux.c']]],
  ['savestatus_1',['saveStatus',['../deux_8c.html#a3bba1ba5f848dde11fbc93cab5278878',1,'saveStatus(int status):&#160;deux.c'],['../deux_8h.html#a3bba1ba5f848dde11fbc93cab5278878',1,'saveStatus(int status):&#160;deux.c']]],
  ['savevilles_2',['saveVilles',['../un_8c.html#a8d67ce9f6c4a989685fed049047d29e9',1,'saveVilles(VilleIUT *tiut[], int nbVilles):&#160;un.c'],['../un_8h.html#a8d67ce9f6c4a989685fed049047d29e9',1,'saveVilles(VilleIUT *tiut[], int nbVilles):&#160;un.c']]],
  ['structures_2eh_3',['structures.h',['../structures_8h.html',1,'']]],
  ['suiv_4',['suiv',['../struct_maillon_dept.html#a7a1769666f1fea0a825997a2d123f870',1,'MaillonDept']]],
  ['suivant_5',['suivant',['../struct_maillon.html#aea95d83b27a1bdd2abe97732f12f3421',1,'Maillon']]],
  ['supprimerdepartement_6',['supprimerDepartement',['../un_8c.html#a28d5da430784efeb0ba2a4fea3a506bc',1,'supprimerDepartement(ListeDept ldept, char *searchDept):&#160;un.c'],['../un_8h.html#a28d5da430784efeb0ba2a4fea3a506bc',1,'supprimerDepartement(ListeDept ldept, char *searchDept):&#160;un.c']]]
];
