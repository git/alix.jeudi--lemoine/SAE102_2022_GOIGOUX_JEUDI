.PHONY: doc

all :
	make -C src all

doc :
	make -C doc all

install :
	@cp -R src/IUT src/donnees .
	@chmod +x IUT
	@printf '\033[0;32mVous pouvez maintenant lancer le programme en faisant ./IUT\033[0m\n'

clean :
	rm -rf IUT donnees/
	make -C src clean
	make -C doc clean
