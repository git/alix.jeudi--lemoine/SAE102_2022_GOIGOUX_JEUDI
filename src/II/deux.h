/*! 
	\file deux.h
	\author GOIGOUX Lucie & JEUDI--LEMOINE Alix
	\date 23/12/22
	\brief Partie 2 de la SAE 1.02

	Application de gestion des candidature dans les IUT de France
*/

//FONCTIONS ET DOCUMENTATION

/**
    \brief Alloue de la mémoire de la taille d'une structure Candidat avec un malloc
    \return Retourne l'adresse générée par le malloc 
*/
Candidat* creerCandidat(void);

/**
    \brief Alloue de la mémoire de la taille d'une structure Candidature avec un malloc avec les valeurs par défaut à 0 pour les variables décision & validation
    \return Retourne l'adresse générée par le malloc 
*/
Candidature* creerCandidature(void);

/**
    \brief Ajoute l'adresse du candidat dans le tableau de pointeur "liste".
    \param listePointer Adresse du tableau de pointeur sur la structure Candidat
    \param nbCandidats Adresse de la variable contenant le nombre de candidats
    \param candidat Adresse du Candidat à ajouter
*/
void ajouterCandidat(ListeCandidats* listePointer, int* nbCandidats, Candidat* candidat);

/**
    \brief Ajoute une candidature dans la liste des candidatures associées au candidat correspondant dans les paramètres de la fonction 
    \param candidat Adresse du candidat dont on veut ajouter une candidature
    \param candidature Adresse de la candidature à ajouter
*/
void ajouterCandidature(Candidat* candidat, Candidature* candidature);

/**
  \brief Permet au candidat de déposer une candidature
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes ayant un IUT
  \param candidat Adresse du candidat qui veut déposer une candidature
*/
void menuCandidat_candidater(VilleIUT** tiut, int nbVilles, Candidat* candidat);

/**
    \brief Verifie si la candidature à ajouter n'existe pas déjà
    \param candidat Adresse du candidat auquel on veut ajouter une candidature
    \param candidature Candidature à vérifier
    \return Retourne -1 si la candidature existe déjà et 0 sinon
*/
int checkCandidature(Candidat* candidat, Candidature candid);


/**
    \brief Affiche tous les candidats triés par nom
    \param liste Tableau de pointeur sur la structure Candidat
    \param nbCandidats Nombre de candidats dans la liste des candidats
*/
void afficherListeCandidats(ListeCandidats liste, int nbCandidats);

/**
    \brief Affiche un candidat précis
    \param candidat Adresse du candidat que l'utilisateur choisi d'afficher
*/
void afficherCandidat(Candidat* candidat);

/**
    \brief Affiche récursivement toutes les candidatures d'un seul candidat
    \param candidatures Tableau de pointeur sur les candidatures d'un candidat
    \param nbCandidatures Nombre de candidatures du candidat
*/
void afficherCandidatures(ListeCandidatures candidatures, int nbCandidatures);

/**
    \brief Lit la liste des candidats dans le fichier texte candidats.don puis les met dans la structure ListeCandidats
    \param nbCandidats Adresse de la variable qui va contenir le nombre de candidats dans le fichier candidats.don
    \return Renvoie la liste des candidats
*/
ListeCandidats readCandidats(int* nbCandidats);

/**
    \brief Enregistre la liste des candidats dans le fichier texte candidats.don
    \param liste Tableau de pointeur sur la structure Candidat
    \param nbCandidats Nombre de candidats dans la liste des candidats
    \return Renvoie -1 si il y a eu un problème de lecture du fichier et 0 si tout s'est bien passé
*/
int saveCandidats(ListeCandidats liste, int nbCandidats);

/**
    \brief Compare deux candidats suivant l'ordre alphabetique
    \param c1 Candidat 1
    \param c2 Candidat 2
    \return <0 si le candidat 1 est avant le candidat 2 dans l'ordre alphabetique, >0 si le candidat 2 est avant le candidat 1 dans l'ordre alphabetique et 0 si ils sont homonymes
*/
int compareCandidats(const void* c1, const void* c2);

/**
  \brief Lis le status actuel de la phase de candidature (0 si la phase est arrêtée et 1 si elle est lancée)
  \return Retourne le status actuel de la phase de candidature 
*/ 
int readStatus(void);

/**
  \brief Enregistre sur le fichier texte status.don le status actuel de la phase de candidature (0 si la phase est arrêtée et 1 si elle est lancée)
  \param status le status actuel de la phase de candidature 
*/ 
void saveStatus(int status);