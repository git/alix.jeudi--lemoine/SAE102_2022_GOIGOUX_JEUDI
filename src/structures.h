#define LEN_MAX 30

// STRUCTURES PARTIE 1
typedef struct MaillonDept {
  char departement[LEN_MAX];
  int nbP;
  char responsable[LEN_MAX];
  float noteMinimale; // Ajout Partie III
  struct MaillonDept* suiv;
} MaillonDept;

typedef MaillonDept* ListeDept;

typedef struct {
  char ville[LEN_MAX];
  ListeDept ldept;
} VilleIUT;

// STRUCTURES PARTIE 2
typedef struct {
  char ville[LEN_MAX], departement[LEN_MAX];
  int decision, validation;
} Candidature;

typedef Candidature** ListeCandidatures;

typedef struct {
  int id;
  char nom[LEN_MAX], prenom[LEN_MAX];
  float moyenneMath, moyenneFrancais, moyenneAnglais, moyenneSpecialite, noteGlobale; // noteGlobale ajoutée Partie III
  int nbCandidatures;
  ListeCandidatures listeCandidatures;
} Candidat;

typedef Candidat** ListeCandidats;

// STRUCTURES PARTIE IV

typedef struct {
  char nom[LEN_MAX], prenom[LEN_MAX];
  float moyenneMath, moyenneFrancais, moyenneAnglais, moyenneSpecialite, noteGlobale;
} CandidatTraite;

typedef struct Maillon {
  CandidatTraite candidat;
  struct Maillon *suivant;
} Maillon;

typedef struct {
  Maillon *tete;
  Maillon *queue;
  int nbAttente;
} FileAttente;

typedef CandidatTraite** ListeAdmis;