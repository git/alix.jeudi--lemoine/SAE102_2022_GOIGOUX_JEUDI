/*! 
	\file quatre.h
	\author GOIGOUX Lucie & JEUDI--LEMOINE Alix
	\date 12/01/23
	\brief Partie 4 de la SAE 1.02

	Application de gestion des candidature dans les IUT de France
*/


/**
    \brief Permet de voir les candidatures et les décisions prises
    \param liste Tableau de pointeur sur la structure Candidat 
    \param nbCandidats Nombre de candidats dans la liste des candidats
	\param candidat Candidat en question
*/
void menuCandidat_suivreCandidature(ListeCandidats liste, int nbCandidats, Candidat* candidat); 


/**
    \brief Permet d'accepter un certain nombre de candidat ayant une note minimale 
    \param tiut Tableau de pointeur sur la structure VilleIUT 
    \param nbVilles Nombre de villes
	\param liste Tableau de pointeur sur la structure Candidat 
	\param nbCandidat Candidat en question
*/
void menuResponsable_traiterCandidatures(VilleIUT** tiut, int nbVilles, ListeCandidats liste, int nbCandidats);

FileAttente* creerFile(void); // Retourne address FileAttente malloc
int estVide(FileAttente *file);// Retourne 1 ou 0 si vide
void enfiler(FileAttente *file, CandidatTraite candidat); // Enfile candidat dans file
CandidatTraite defiler(FileAttente *file); // Defile candidat et le renvoie

FileAttente* lireCandidatsAttente(char* dept, char* ville); // Lis le fichier Dpt_Ville_Attente.don et retourne FileAttente* remplie
void ecrireCandidatsAttente(char* dept, char* ville, FileAttente* file);

int compareCandidatsTraite(const void* p1, const void* p2); // Pour utiliser le qsort

void ajouterCandidatAdmis(char* dept, char* ville, CandidatTraite* candidat);

void afficherCandidatsAdmis(char* dept, char* ville);
void afficherCandidatsAttente(FileAttente* file);