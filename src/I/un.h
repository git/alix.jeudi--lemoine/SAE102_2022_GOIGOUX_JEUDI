/*! 
	\file un.h
	\author GOIGOUX Lucie & JEUDI--LEMOINE Alix
	\date 22/12/22
	\brief Partie 1 de la SAE 1.02

	Application de gestion des candidature dans les IUT de France
*/

/**
  \brief Propose à l'utilisateur de se connecter avec son id ou de créer son compte étudiant. Appelle la fonction créerCandidat si l'utilisateur veut s'enregistrer.
  \param id Passage par adresse de la variable "id"
  \param listePointer Adresse de la variable contenant le tableau de pointeur sur la structure candidat
  \param nbcandidat Pointeur sur le nombre de candidats dans le fichier candidat.don
*/
void identificationCandidat(int* id, ListeCandidats* listePointer, int* nbCandidats);

/**
  \brief Affiche le titre "RECRUTEMENT IUT"
 */
void titreMenuPrincipal(void);

/**
  \brief Affiche le menu des candidats et permet l'appel des fonctions suivant le choix de l'utilisateur
  \param tiutPointer Adresse de la variable contenant le tableau de pointeur sur la structure VilleIUT
  \param listePointer Adresse de la variable contenant le tableau de pointeur sur la structure candidat
  \param nbVilles Pointeur sur le nombre de villes dans le tableau tiut
  \param nbcandidat Pointeur sur le nombre de candidats dans le tableau liste
*/
void menuCandidat(VilleIUT** tiutPointer, ListeCandidats* listePointer, int nbVilles, int* nbCandidats);

/**
  \brief Affiche le menu des responsables et permet l'appel des fonctions suivant le choix de l'utilisateur
  \param tiutPointer Adresse de la variable contenant le tableau de pointeur sur la structure VilleIUT
  \param listePointer Adresse de la variable contenant le tableau de pointeur sur la structure Candidat
  \param nbVilles Pointeur sur le nombre de villes dans le tableau tiut
  \param nbCandidats Pointeur sur le nombre de candidats dans le tableau liste
*/
void menuResponsable(VilleIUT** tiut, ListeCandidats liste, int nbVilles, int nbCandidats);

/**
  \brief Affiche le menu des administrateurs et permet l'appel des fonctions suivant le choix de l'utilisateur
  \param tiutPointer Adresse de la variable contenant le tableau de pointeur sur la structure VilleIUT
  \param listePointer Adresse de la variable contenant le tableau de pointeur sur la structure Candidat
  \param nbVilles Pointeur sur le nombre de villes dans le tableau tiut
  \param nbcandidat Pointeur sur le nombre de candidats dans le tableau liste
*/
void menuAdmin(VilleIUT*** tiutPointer, ListeCandidats liste, int* nbVilles, int nbCandidats);


/**
  \brief Affiche le menu principal et permet l'appel des menus candidats/administrateurs/responsables suivant le choix de l'utilisateur
*/
void menuPrincipal(void);


/**
  \brief Fonction permettant à l'administrateur de changer le nombre de places disponibles dans une formation
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
*/
void menuAdmin_modifierPlaces(VilleIUT** tiut, int nbVilles);

/**
  \brief BONUS: Fonction permettant à l'utilisateur administrateur de créer un nouvel IUT 
  \param tiutPointer Adresse de la variable contenant le tableau de pointeur sur la structure VilleIUT
  \param nbVilles Pointeur sur le nombre de villes dans le tableau tiut
*/
void menuAdmin_creerIUT(VilleIUT*** tiutPointer, int* nbVilles);

/**
  \brief Fonction permettant à l'utilisateur administrateur de créer un nouveau département
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
*/
void menuAdmin_creerDepartement(VilleIUT** tiut, int nbVilles);

/**
  \brief Fonction permettant à l'utilisateur administrateur de supprimer un département existant
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
*/
void menuAdmin_supprimerDepartement(VilleIUT** tiut, int nbVilles);

/**
  \brief Fonction permettant à l'utilisateur administrateur de modifier le nom d'un responsable d'un département
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
*/
void menuAdmin_modifierResponsable(VilleIUT** tiut, int nbVilles);

/**
  \brief BONUS: Alloue de la memoire de la taille d'une structure VilleIUT avec un malloc
  \return retourne l'adresse générée par le malloc
*/
VilleIUT* creerVille(void);

/**
  \brief BONUS: ajoute l'adresse d'une ville dans le tableau tiut contenue à l'adresse tiutPointer. On passe en paramètre tiutPointer parce que le realloc peut changer l'adresse mémoire du tableau
  \param tiutPointer Adresse de la variable contenant le tableau de pointeur sur la structure VilleIUT
  \param ville Adresse de la nouvelle ville à ajouter dans le tableau tiut
  \param nbVilles Pointeur sur le nombre de villes dans le tableau tiut. On ajoute +1 à la variable.
*/
void ajouterVille(VilleIUT*** tiutPointer, VilleIUT* ville, int* nbVilles);

/**
  \brief Fonction de recherche pour trouver la position de la ville recherchée dans le tableau tiut
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
  \param searchIUT Ville recherchée dans le tableau
  \return La fonction retourne l'index de la ville recherchée dans le tableau tiut ou -1 si introuvable
*/
int rechercherVille(VilleIUT** tiut, int nbVilles, char* searchIUT); // Retourne index de la ville

/**
  \brief Affiche la liste des villes ayant un IUT
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
*/
void afficherListeVilles(VilleIUT** tiut, int nbVilles);

/**
  \brief Créer une liste vide
  \return Retourne NULL
*/
ListeDept creerListeDepartement(void);

/**
  \brief Affiche la liste des différents départements disponible dans une ville recherchée et le nombre de places dans la formation
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
  \param searchIUT Ville recherchée par l'utilisateur
  \return 1 si l'affichage s'est bien déroulé et -1 si la ville recherchée n'existe pas 
*/
int afficherListeDepartement(VilleIUT** tiut, int nbVilles, char* searchIUT);

/**
  \brief Affiche la liste des IUT comprenant un département recherché par l'utilisateur
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
  \param searchDept Département recherchée par l'utilisateur
*/
void afficherDepartement(VilleIUT** tiut, int nbVilles, char* searchDept);

/**
  \brief Alloue de la memoire de la taille d'une structure MaillonDept avec un malloc
  \param departement Nom du nouveau département à créer
  \param nbP Nombre de place disponible dans le nouveau département à ajouter
  \param responsable Nom du responsable pour le nouveau département
  \return Retourne l'adresse du nouveau département
*/
MaillonDept* creerDepartement(char* departement, int nbP, char* responsable);

/**
  \brief Ajoute un département dans la liste chaînée des départements d'une ville
  \param ldept Liste chaînée des départements d'un IUT précis
  \param dept Adresse du département à ajouter
  \return Retourne la liste des département de la ville (adresse)
*/
ListeDept ajouterDepartement(ListeDept ldept, MaillonDept* dept);

/**
  \brief Supprime le département dans une ville choisie par l'utilisateur
  \param ldept Liste chaînée des départements d'un IUT précis
  \param searchDept Département recherchée par l'utilisateur
  \return Retourne la nouvelle liste des département pour la ville en question
*/
ListeDept supprimerDepartement(ListeDept ldept, char* searchDept);

/**
  \brief Modifie le nombre de place d'une formation
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
  \param searchDept Département recherchée par l'utilisateur
  \param nb Nouveau nombre de places disponibles pour la formation
*/
void modifierNbPlaces(VilleIUT** tiut, int nbVilles, char* searchIUT, char* searchDept, int nb);

/**
  \brief Modifie le nom du responsable d'une formation
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
  \param searchIUT Ville recherchée par l'utilisateur
  \param searchDept Département recherchée par l'utilisateur
  \param nom Nom du nouveau responsable pour la formation choisie par l'utilisateur
*/ 
void modifierNomResponsable(VilleIUT** tiut, int nbVilles, char* searchIUT, char* searchDept, char* nom);

/**
  \brief Enregistre les modifications apportées aux structures de données dans le fichier binaire ville.bin
  \param tiut Tableau de pointeur sur la structure VilleIUT
  \param nbVilles Nombre de villes dans le tableau tiut
*/ 
void saveVilles(VilleIUT* tiut[], int nbVilles);

/**
  \brief Lit le fichier binaire ville.bin et retourne le tableau tiut avec les structures de données et les données du fichier dans ce tableau
  \param nbVilles Adresse de la variable qui va contenir le nombre de villes dans le fichier binaire villes.bin
*/ 
VilleIUT** readVilles(int* nbVilles);