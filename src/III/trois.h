/*! 
	\file deux.h
	\author GOIGOUX Lucie & JEUDI--LEMOINE Alix
	\date 06/01/23
	\brief Partie 3 de la SAE 1.02

	Application de gestion des candidature dans les IUT de France
*/

//FONCTIONS ET DOCUMENTATION

/**
    \brief Calcul la note globale en fonction des coéfficients prédéfinis
    \param liste Tableau de pointeur sur la structure Candidat
    \param nbCandidats Nombre de candidats dans la liste des candidats
*/
void calculerNoteGlobale(ListeCandidats liste, int nbCandidats);

/**
    \brief Affiche les candidatures pour un seul département dans un IUT
    \param liste Tableau de pointeur sur la structure Candidat 
    \param nbCandidats Nombre de candidats dans la liste des candidats
	\param searchIUT Nom de l'IUT en question
	\param searchDept Nom du département en question
*/
void afficherCandidaturesByDept(ListeCandidats liste, int nbCandidats, char* searchIUT, char* searchDept);

/**
    \brief Fonction de tri dichotomique
    \param liste Tableau de pointeur sur la structure Candidat 
    \param start 
	\param middle
	\param end 
*/
void merge(ListeCandidats liste, int start, int middle, int end);

/**
    \brief Algorithme de tri dichotomique
    \param liste Tableau de pointeur sur la structure Candidat 
    \param start début du tableau à trier
	\param end nombre d'élements dans le tableau à trier
*/
void triDichotomiqueFusion(ListeCandidats liste, int start, int end);

/**
    \brief Affiche les information d'un candidat
    \param candidat Candidat en question
*/
void afficherCandidatTraite(Candidat candidat);

/**
	\brief Permet de traiter les candidatures et met les candidats dans des fichiers txt suivant s'ils sont admis ou en liste d'attente
	\param liste Tableau de pointeur sur la structure Candidat 
	\param nbCandidats Nombre de candidats dans la liste des candidats
	\param nbCandidatsAccept Nombre de candidats qui vont être accéptés pendant ce traitement, le responsable détermine ce nombre
	\param noteMini Moyenne minimal que le candidat doit avoir si il veut être admis, le responsable détermine cette note
	\param checkQueue Contient 1 s'il faut vérifier le contenu de la file d'attente, 0 sinon
*/
void traiterCandidatures(ListeCandidats liste, int nbCandidats, int nbCandidatsAccept, float noteMini, int checkQueue);