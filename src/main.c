/*! 
	\file main.c
	\author GOIGOUX Lucie & JEUDI--LEMOINE Alix
	\date 15/12/22
	\brief Exécutable principal du programme

	Application de gestion des candidature dans les IUT de France
*/

#include "structures.h"
#include "I/un.c"

int main(void) {
    menuPrincipal();
	
    return 0;
}