# Application de gestion des candidature dans les IUT de France

## Prérequis
Vous devez disposer de gcc et git sur votre machine pour télécharger et utiliser ce programme

## Instructions d'installation
Pour compiler, installer et lancer ce programme sur votre propre machine, vous devez suivre les instructions suivantes :
<details>
    <summary>1. Cloner le dépot</summary>
    <p>git clone https://codefirst.iut.uca.fr/git/alix.jeudi--lemoine/SAE102_2022_GOIGOUX_JEUDI</p>
</details>
<details>
    <summary>2. Entrer dans le dossier</summary>
    <p>cd SAE102_2022_GOIGOUX_JEUDI</p>
</details>
<details>
    <summary>3. Compiler le programme</summary>
    <p>make</p>
</details>
<details>
    <summary>4. Installer le programme</summary>
    <p>make install</p>
</details>
<details>
    <summary>5. Lancer le programme</summary>
    <p>./IUT</p>
</details>

## Comment générer la documentation
Pour générer la documentation de ce programme, vous devez suivre les instructions suivantes :
<details>
    <summary>1. Cloner le dépot</summary>
    <p>git clone https://codefirst.iut.uca.fr/git/alix.jeudi--lemoine/SAE102_2022_GOIGOUX_JEUDI</p>
</details>
<details>
    <summary>2. Entrer dans le dossier</summary>
    <p>cd SAE102_2022_GOIGOUX_JEUDI</p>
</details>
<details>
    <summary>3. Générer la documentation</summary>
    <p>make doc</p>
</details>
<details>
    <summary>4. Entrer dans le dossier doc/html</summary>
    <p>cd doc/html</p>
</details>
<details>
    <summary>5. Ouvrir le fichier index.html avec le navigateur de votre choix</summary>
    <p>Exemple : firefox-esr index.html</p>
</details>

SAE 1.02 BUT Info, 1A Groupe 3
GOIGOUX Lucie,
JEUDI--LEMOINE Alix. 